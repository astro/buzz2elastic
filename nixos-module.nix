{ self }:
{ config, lib, pkgs, ... }: {
  options.services.buzz2elastic = with lib; {
    enable = mkEnableOption "Fedi.buzz to Elastic";
  };

  config =
    let
      cfg = config.services.buzz2elastic;
      inherit (self.packages.${pkgs.system}) buzz2elastic;
    in
      lib.mkIf cfg.enable {
        systemd.services.buzz2elastic = {
          wantedBy = [ "multi-user.target" ];
          after = [ "network-online.target" ];
          serviceConfig = {
            Type = "notify";
            ExecStart = "${buzz2elastic}/bin/buzz2elastic";
            DynamicUser = true;
            ProtectSystem = "full";
            Restart = "always";
            RestartSec = "1s";
            WatchdogSec = "1800s";
          };
        };
      };
}
